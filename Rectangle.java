import java.util.Scanner; 

public class Rectangle{
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in); 
        int counter = 0; 
        int widthInput = 0; 
        int heightInput = 0; 

        while (counter >= 0){
        //while-loop to keep re-promting for user input until user chooses to quit. 

            while (counter == 0){
                counter = 1; 
                System.out.println("Enter the rectangles width. Press q to quit");
    
                String input = scan.nextLine();
                //Get user input
                if (input.equals("q")){
                    counter = -1; 
                    //if user entered q as input: escapes outer while-loop. 
                    
                }
                else {
                    try {
                        widthInput = Integer.parseInt(input);
                        //Checking if user input is an integer
                    }catch(NumberFormatException ex) {
                        System.out.println("You did not enter a number. Try again");
                        counter = 0; 
                        //If not continue while loop, until user enters a valid input. 
                    }
                }
            }
    
            while (counter == 1){
                counter = 2; 
    
                System.out.println("Enter the rectangles height. Press q to quit");
    
                String nextInput = scan.nextLine();
                if (nextInput.equals("q")){
                    counter = -1; 
                }
                else{
                    try {
                        heightInput = Integer.parseInt(nextInput);
                    }catch(NumberFormatException ex) {
                        System.out.println("You did not enter a number. Try again");
                        counter = 1; 
                    }
                }   
            }

            if (counter >= 0){
                makeRectangle(widthInput, heightInput);
                makeNestedRectangles(widthInput, heightInput);
                //if user has not chosen to end program: call on methods to draw rectangles 
                //with user input as width and height.
                counter = 0; 
                //keep while loop going until user chooses to quit. 
            }  
        }
    }


    static void makeRectangle(int width, int height){
        char myChar = '#'; 

        System.out.println("Rectangle of size: " + width + " by " + height + ":"); 

        for (int i = 0; i < height; i++){
            for (int j = 0; j < width; j++){
                if (i == 0 || i == height -1){
                    System.out.print(myChar);
                    //In top or bottom height, all elements in width should be myChar. 
                }
                else{
                    if (j == 0 || j == width -1){
                        System.out.print(myChar);
                        //In both ends of width, elements should be myChar. 
                    }
                    else {
                        System.out.print(" "); 
                        //Evereywhere else, elements should be blank-space char. 
                    }
                }
            }
            System.out.print("\n"); 
            //Go to new line for each height.
        }
    }


    static void makeNestedRectangles(int width, int height){
        char myChar = '#'; 
        int innerWidth = width - 4; 
        int innerHeight = height - 4; 

        if (innerWidth < 1 || innerHeight < 1){
            return;
            //if inner rectangle size is smaller than 1 by 1, there cannot be drawn an inner rectangle.
            //returns without printing. 
        }

        System.out.println("Nested rectangles of sizes: " + width + " by " + height + ", and " + innerWidth + " by " + innerHeight + ":"); 
        
        for (int i = 0; i < height; i++){
            for (int j = 0; j < width; j++){
                if (i == 0 || i == height -1 ){
                    System.out.print(myChar);
                    //At top and bottom height, all elements in width should be myChar. 
                }
                else if (i == 1 || i == height -2){
                    //At the next line/height from top and bottom 
                    if (j == 0 || j == width -1){
                        System.out.print(myChar);
                        //In both ends of width, elemets should be myChar. 
                    }
                    else{
                        System.out.print(" "); 
                        //rest of elements as blank-space chars. 
                    }
                }
                else if (i == 2 || i == height -3){
                    //At the third line/height from top and bottom
                    if (j == 0 || j == width -1){
                        System.out.print(myChar); 
                        //In both ends of width, elemets should be myChar.
                    }
                    else if (j == 1 || j == width -2){
                        System.out.print(" "); 
                        //elements next to the ends of the width should be blank-space chars. 
                    }
                    else{
                        System.out.print(myChar); 
                        //rest of the elements forming the top and bottom inner rectangle should be myChar. 
                    }
                }
                else {
                    //The rest of the lines/heights
                    if (j == 0 || j == width -1){
                        System.out.print(myChar); 
                        //In both ends of width, elemets should be myChar.
                    }
                    else if (j == 1 || j == width -2){
                        System.out.print(" "); 
                        //elements next to the ends of the width should be blank-space chars.
                    }
                    else if (j == 2 || j == width -3){
                        System.out.print(myChar); 
                        //The third elements in width should be myChar. 
                    }
                    else{
                        System.out.print(" "); 
                        //rest of the elements as blank-space chars. 
                    }
                }
            }
            System.out.print("\n"); 
            //Go to new line for each height.
        }
    }

       
}